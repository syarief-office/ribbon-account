/* Author:http://www.rainatspace.com

*/

function closeArea(btn, target){
	jQuery(btn).on('click', function(){
		jQuery(this).closest(target).slideUp(200);
		return false;
	});
}
function initializeScript(){
	//jQuery('#preloader').fadeOut(2000);
		jQuery("[icon-data]").each(function(){
		var getAttr =  jQuery(this).attr('icon-data');
		jQuery(this).addClass(getAttr).removeAttr('icon-data');
	});

	// Navigation  Bounce Menu Effect================================================================*/
	jQuery('.nav li ul').removeClass('hidden');
		jQuery('.nav li').hover(function() {
			jQuery('ul', this).filter(':not(:animated)').slideDown(600, 'easeOutBounce');
	     }, function() {
		jQuery('ul', this).slideUp(600, 'easeInExpo');
	});

	// NAVIGATION RESPOSNIVE HANDLER
	jQuery(".nav >  ul").clone(false).appendTo(".nav-rwd-sidebar");
	jQuery(window).on('load', function(){
		jQuery('.nav-rwd-sidebar').find('ul').removeClass();
	});
	jQuery(".btn-rwd-sidebar, .btn-hide").click( function() {
		jQuery(".nav-rwd-sidebar").toggleClass("sidebar-active");
		jQuery(".wrapper-inner").toggleClass("wrapper-active");
	});

	//EDITING FORM 
	jQuery('.edit_link').on('click', function(){
		var target = jQuery(this).prev('.form-control');
		if(target.prop('disabled') == 1){
			target.removeProp('disabled');
			jQuery(this).text('Save');
		}else{
			target.prop('disabled', true);
			jQuery(this).text('Edit');
		}
		return false;
	});
	closeArea('.closeBtn', '.fieldbox-wt');
	closeArea('.closeRow', 'tr');

	//HIDE AND SHOW ADD FIELD
	jQuery('.navFade_Form').on('click', function(){
		var $target = jQuery(this).closest('.boxFade-wrap');
		$target.find('.fadeBox').fadeOut(100, function(){
			$target.find('.fade_form').fadeIn(600);
		});

		if($target.find('.fade_form').is(':visible')){
			$target.find('.fade_form').fadeOut(100);
			$target.find('.fadeBox').fadeIn(600);
			jQuery('.navFade_Form').html('<span>Add New</span>');
		}else{
			jQuery('.navFade_Form').html('<i class="fa fa-times"></i>');
		}

		return false;
	});

	jQuery(window).on('load', function(){
		if(jQuery('.nav_payment').length == 1){
			jQuery('.nav_payment').closest('.main-boxwhite').css('padding-top', '110px');
		}
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();
});
/* END ------------------------------------------------------- */